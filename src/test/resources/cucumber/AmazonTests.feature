Feature: Shipping Cart of Amazon
  As an amazon user
  I want to add items in my shopping cart
  Because I want to manage items before I check out

  Background:
    Given I am on amazon home page

  @AdddItem
  Scenario: Verify youtube logo on home page
    Given I search for "Harry Potter" book
    And I add the first book to my cart
    And I see the message "Added to Cart" with all details
    When I check my shopping cart
    Then I should see the book in my shopping cart


