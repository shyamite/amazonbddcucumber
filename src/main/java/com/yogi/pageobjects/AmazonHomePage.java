package com.yogi.pageobjects;

import com.yogi.pageobjectcommands.AmazonResultPageCommands;

public class AmazonHomePage {
    public static AmazonResultPageCommands goTo(){
        return new AmazonResultPageCommands();
    }
}
