package com.yogi.pageobjects;

import com.yogi.pageobjectcommands.ShoppingCartPageCommands;

public class ShoppingCartPage {

    public static ShoppingCartPageCommands goTo(){
        return new ShoppingCartPageCommands();
    }
}
