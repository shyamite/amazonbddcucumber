package com.yogi.exceptions;

public class ItemIsNotExistOnPage extends Exception {
    public ItemIsNotExistOnPage() {}

    // Constructor that accepts a message
    public ItemIsNotExistOnPage(String message)
    {
        super(message);
    }
}
